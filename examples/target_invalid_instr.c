#include <string.h>
#include <stdlib.h>
#include <sys/mman.h>

int main(int argc, char** argv) {
    int data_len = 0x5;
    char* data = malloc(data_len * sizeof(char));
    if (!mprotect(data, data_len, PROT_READ | PROT_WRITE | PROT_EXEC)) {
        return 1;
    }
    char* raw_data = "\x06\x90\xcc\x90\xcc";
    memcpy(data, raw_data, data_len);
    void (*fn)() = (void*)data;
    fn();
}
