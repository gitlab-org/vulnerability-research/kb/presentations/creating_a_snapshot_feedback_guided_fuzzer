use std::env::args;
use std::os::unix::process::CommandExt;
use std::process::exit;
use std::process::Command;

use nix::sys::ptrace;
use nix::sys::signal::Signal;
use nix::sys::wait::{waitpid, WaitStatus};
use nix::unistd::Pid;

pub fn spawn(cmd: &String, args: &[String]) -> Pid {
    let child = unsafe {
        println!("Spawning {}", cmd);
        Command::new(cmd)
            .args(args)
            .pre_exec(|| {
                ptrace::traceme().expect("Could not trace process");
                Ok(())
            })
            .spawn()
            .expect(&format!("Could not spawn {:?} with args {:?}", cmd, args))
    };

    let res = Pid::from_raw(child.id() as i32);

    match waitpid(res, None) {
        Ok(WaitStatus::Stopped(_, Signal::SIGTRAP)) => {
            ptrace::cont(res, None).expect("Should have continued");
        }
        _ => println!("COULD NOT START"),
    }

    res
}

fn main() {
    let prog_args: Vec<String> = args().collect();
    if prog_args.len() == 1 {
        println!("USAGE: {} CMD [CMD_ARG1] [CMD_ARG2]", prog_args[0]);
        exit(1);
    }

    let child_pid = spawn(&prog_args[1], &prog_args[2..]);
    match waitpid(child_pid, None) {
        Ok(WaitStatus::Exited(_, status)) => {
            println!("Process exited with status {}", status);
        }
        Ok(WaitStatus::Stopped(_, Signal::SIGTRAP)) => {
            println!("Debugee stopped");
            ptrace::cont(child_pid, None).expect("Should have continued");
        }
        Ok(WaitStatus::Continued(_)) => println!("Continuing"),
        Ok(WaitStatus::PtraceEvent(_, signal, v)) => {
            println!("ptrace event: signal: {:?}, v: {:?}", signal, v);
        }
        Ok(WaitStatus::Signaled(_, signal, val)) => {
            println!("ptrace signaled: signal: {:?}, val: {:?}", signal, val);
        }
        Ok(WaitStatus::StillAlive) => println!("Still alive"),
        Ok(WaitStatus::Stopped(_, signal)) => {
            println!("Stopped: signal: {:?}", signal);
        }
        Ok(WaitStatus::PtraceSyscall(_)) => println!("Syscall"),
        Err(v) => println!("Error!: {:?}", v),
    }
    let _ = ptrace::kill(child_pid);
}
