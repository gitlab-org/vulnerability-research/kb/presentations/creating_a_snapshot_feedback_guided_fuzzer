#include <string.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) {
    char *data = "hello world";
    FILE *fd = fopen("/proc/self/mem", "r+");
    fseek(fd, (size_t) data, SEEK_SET);

    char *new_data = malloc(strlen(data) + 1);
    fread(new_data, sizeof(char), strlen(data) + 1, fd);
    fwrite("blarg", sizeof(char), strlen(data) + 1, fd);

    printf("Read data from ptr %p from /proc/self/mem: %s\n", data, new_data);
}
