# Creating A Snapshot-Based & Feedback-Guided Fuzzer

This project holds the example code for the
[Creating a Snapshot-based, Feedback-Driven Fuzzer](https://gitlab.com/gitlab-org/secure/brown-bag-sessions/-/issues/33)
Brown Bag Session.

## Links

* [Video](https://youtu.be/fGF-tEXwW7U)

[![Title Slide](title_slide.png)](https://youtu.be/fGF-tEXwW7U)

## Usage

```
git clone --recurse-submodules git@gitlab.com:gitlab-org/vulnerability-research/kb/presentations/creating_a_snapshot_feedback_guided_fuzzer.git
```

## Requirements

You'll need gcc, clang, and rust build environments installed.
